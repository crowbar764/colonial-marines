package io;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import entity.Entity;
import entity.effect.Bullet;
import entity.mob.Mob;
import game.DEFINES;
import game.Game;
import render.GUI;
import render.Renderer;
import world.Camera;
import world.World;

public class Mouse implements MouseListener{
	

	public static Point getMouseWindowPos() {
		
		Point mousePos = MouseInfo.getPointerInfo().getLocation();
		Point windowPos = Game.frame.getLocationOnScreen();
		int xPos = (mousePos.x - (int)windowPos.getX()) - Game.pixelRes / 4; // why??
		int yPos = (mousePos.y - (int)windowPos.getY()) - Game.pixelRes;
		return new Point(xPos, yPos);
	}
	
	public static Point getMouseWorldPos() {
		
		Point windowPos = getMouseWindowPos();
		Point worldPos = new Point((int)((windowPos.x + (Camera.x * Renderer.zoom)) / Renderer.zoom), (int)((windowPos.y + (Camera.y * Renderer.zoom)) / Renderer.zoom));
		
		return worldPos;
	}
	
	// For click mask, since content changes but only covers screen.
	public static Point getMouseWorldPosNoCamera() {
		
		Point windowPos = getMouseWindowPos();
		Point worldPos = new Point((int)(windowPos.x / Renderer.zoom), (int)(windowPos.y / Renderer.zoom));
		
		return worldPos;
	}
	
	public static void mouseClick(MouseEvent e) {
		int button = e.getButton();
		if(button == 1) { // Left click
			
			SSSelect.deselect();
			SSSelect.addEntities(ClickMask.getTarget(getMouseWorldPosNoCamera()));
			
		} else if(button == 3) { // Right click
			
			Point WorldPos = getMouseWorldPos();
			
			for(Entity entity : SSSelect.selectedEntities) {
				if(entity.entityType == DEFINES.MOB) {
					((Mob)entity).setTargetLocation(World.toGrid(WorldPos.x), World.toGrid(WorldPos.y));
				}
			}
		}
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {

	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	/**
	 * Run when the user pressed the mouse button down, either click or drag.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getButton() == 1) { 
			SSSelect.deselect(); 
	        MouseDrag.startDrag = getMouseWorldPos(); 
	        MouseDrag.endDrag = MouseDrag.startDrag; 
		}
	}

	/**
	 * Run when the user releases the mouse after a click or drag.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		
		if(e.getButton() == 1) { 
			Rectangle selectArea = SSSelect.selectArea; 
			 
			// If drag too small then treat like click instead. 
			if(selectArea.width < 10 && selectArea.height < 10) { 
				mouseClick(e); 
			} 
			 
			SSSelect.selectArea(); 
			SSSelect.selectArea = new Rectangle(0, 0, 0 ,0); 
	        MouseDrag.startDrag = null; 
	        MouseDrag.endDrag = null; 
		} 
		 
		if(e.getButton() == 3) { 
			mouseClick(e); 
		} 
		
		GUI.updateSelectionPanel();
	}
}