package io;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import world.Camera;

public class MouseWheel implements MouseWheelListener {
	
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		Camera.momentum.addMomentum(0, 0, e.getWheelRotation() * -1);
	}
}