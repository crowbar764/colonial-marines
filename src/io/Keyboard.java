package io;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import world.Camera;

public class Keyboard implements KeyListener{
	
	static Boolean[] keyStatus = {
		false,
		false, //W
		false, //A
		false, //S
		false, //D
	};
	
	
	public static int getKey(KeyEvent e) {
		switch (e.getKeyCode()) {
		case 0x57: // W
			return 1;
		case 0x41: // A
			return 2;
		case 0x53: // S
			return 3;
		case 0x44: // D
			return 4;
//		case 0x43: // C
////			Game.test();
//			break;	
//		case KeyEvent.VK_ESCAPE:
//			Game.running = false;
//			break;
//		case KeyEvent.VK_SPACE:
//			//Game.test();
//			break;
		default:
			return 0;
		}
	}
	
	public static void process() {
		if(keyStatus[1]) {
			Camera.momentum.addMomentum(0, -5);
		}
		if(keyStatus[2]) {
			Camera.momentum.addMomentum(-5, 0);
		}
		if(keyStatus[3]) {
			Camera.momentum.addMomentum(0, 5);
		}
		if(keyStatus[4]) {
			Camera.momentum.addMomentum(5, 0);
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyID = getKey(e);
		
		keyStatus[keyID] = true;
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int keyID = getKey(e);
		
		keyStatus[keyID] = false;
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
}