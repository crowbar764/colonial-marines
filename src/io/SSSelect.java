package io;

import java.awt.Rectangle;
import java.util.ArrayList;

import entity.Entity;
import game.Game;
import render.GUI;
import world.Camera;
import world.World;

public class SSSelect {
	
	public static ArrayList<Entity> selectedEntities;
	public static Rectangle selectArea;
	
	public static void addEntities(Entity entity) {
		
		if(!entity.isSelectable) {
			return;
		}
		
		entity.isSelected = true;
		selectedEntities.add(entity);
	}
	
	public static void addEntities(ArrayList<Entity> entities) {
		
		for(Entity entity : entities) {
			entity.isSelected = true;
			selectedEntities.add(entity);
		}
	}
	
	public static void deselect() {
		
		for (Entity entity : selectedEntities) {
			entity.isSelected = false;
		}
		selectedEntities.clear();
	}

	public static void init() {
		
		selectedEntities = new ArrayList<Entity>();
		selectArea = new Rectangle(0, 0, 0 ,0);
	}
	
	public static void selectArea() {
		
		for(int y = selectArea.y / Game.pixelRes; y < ((selectArea.y + selectArea.height) / Game.pixelRes) + 1; y++) {
			for(int x = selectArea.x / Game.pixelRes; x < ((selectArea.x + selectArea.width) / Game.pixelRes) + 1; x++) {
				
				if(x < 0 || y < 0 || x > World.pixelResolution.width + 200 || y > World.pixelResolution.height + 200) { // -1 on last two // TODO uhhhh 200?
					System.out.println("Select box out of world bounds. Aborting.");
					return;
				}
				
				ArrayList<Entity> occupants = World.getTurf(x, y).getOccupants();
				
				if(occupants.isEmpty() == false) {
					for(Entity occupant : occupants) {
						if(occupant.isSelectable) {
							addEntities(occupant);
						}
					}
				}
			}
		}
	}
}