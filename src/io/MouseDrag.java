package io;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

public class MouseDrag implements MouseMotionListener{
	
	public static Point startDrag;
	public static Point endDrag;
	
	/**
	 * Called each time the mouse moves while the button is pressed.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		
		if(startDrag == null) {
			return;
		}
		
		
		
		endDrag = Mouse.getMouseWorldPos();
		
//		System.out.println(startDrag.x - endDrag.x);
		
		
		Point useStart = new Point();
		Point useEnd= new Point();
		
		if(endDrag.x < startDrag.x) {
			useStart.x = endDrag.x;
			useEnd.x = startDrag.x;
		} else {
			useStart.x = startDrag.x;
			useEnd.x = endDrag.x;
		}
		
		if(endDrag.y < startDrag.y) {
			useStart.y = endDrag.y;
			useEnd.y = startDrag.y;
		} else {
			useStart.y = startDrag.y;
			useEnd.y = endDrag.y;
		}
		
		
		
		Rectangle newSelection = new Rectangle(
				useStart.x,
				useStart.y,
				(useEnd.x - useStart.x) + 1,
				(useEnd.y - useStart.y) + 1
				);
		
		if(newSelection.width < 2 && newSelection.height < 2) {
			SSSelect.selectArea = new Rectangle(0, 0, 0 ,0);
		} else {
			SSSelect.selectArea = newSelection;
		}
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}
}