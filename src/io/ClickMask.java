package io;

import java.awt.Point;
import java.awt.image.BufferedImage;

import entity.Entity;
import render.Renderer;
import world.World;

public class ClickMask {
	
	public static BufferedImage mask;

	public static Entity getTarget(Point worldPos) {
		
		mask = Renderer.getMask();
		int entityID = mask.getRGB(worldPos.x, worldPos.y);
		Entity entity = World.getEntity(entityID);
		
		return entity;
	}
}