package atmospherics;

import java.awt.Point;

import entity.turf.Turf;
import game.DEFINES;
import world.World;

public class SSAtmos {

	public static boolean running;

	public static void kill() {
		running = false;
	}

	private static void process(Turf origTurf, Turf turf) {

		int airChange = 1;
		int airCurrent = turf.oxygen;
		if(airChange != 0) {	
			//origTurf.newOxygen = origTurf.oxygen - 50;
			if(airChange + airCurrent > 100) {
				turf.newOxygen = 100;
			} else if(airChange + airCurrent < 0) {
				turf.newOxygen = 0;
			} else {
				turf.newOxygen = airChange + airCurrent;
			}
		}
	}

	private static void needsProcessing(Turf turf, int adjacentTurfX, int adjacentTurfY) {
		if(turf.oxygen > 0) {
			if(adjacentTurfX != -1 && adjacentTurfX != 40 && adjacentTurfY != -1 && adjacentTurfY != 40) { //is not end of map
				Turf adjacentTurf = World.getTurf(new Point(adjacentTurfX, adjacentTurfY));
				if(adjacentTurf.oxygen >= 0 && adjacentTurf.isOpen == true) { //is active turf
					if(turf.oxygen > adjacentTurf.oxygen) { //Should flow

						process(turf, adjacentTurf);
					}
				}
			}
		}
	}


	static Thread thread = new Thread("SSAtmos") {

		public void run(){
			thread.setName("Atmospherics Subsystem");
			System.out.println("Atmospherics running");

			running = true;
			long timer = System.nanoTime();

			while(running) {

				if((System.nanoTime() - timer) / DEFINES.ATMOSTICKRATE >= 1) {
					timer = System.nanoTime();
					
					Turf currentTurf;

					for(int column = 0; column < World.height; column++) {
						for(int row = 0; row < World.width; row++) {

							currentTurf = World.getTurf(new Point(row, column));

							needsProcessing(currentTurf, currentTurf.position.x + 1, currentTurf.position.y);
							needsProcessing(currentTurf, currentTurf.position.x - 1, currentTurf.position.y);
							needsProcessing(currentTurf, currentTurf.position.x, currentTurf.position.y + 1);
							needsProcessing(currentTurf, currentTurf.position.x, currentTurf.position.y - 1);
						}
					}
					for(int column = 0; column < World.height; column++) {
						for(int row = 0; row < World.width; row++) {

							Turf turf = World.getTurf(new Point(row, column));

							turf.oxygen = turf.newOxygen;
						}
					}
//					long remaining = (System.nanoTime() - timer) / 100;
//					System.out.println(remaining);
//					try {
//						Thread.sleep(remaining);
//					} catch (InterruptedException e) {
//						// TODO Auto-generated catch block
//						e.printStackTrace();
//					}
				}
			}
		}
	};

	public static void start() {
		thread.start();
	}
}