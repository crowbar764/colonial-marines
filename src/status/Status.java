package status;

public class Status {
	
//	public int id;
	public String name, description;
	public boolean isGood;
	
	public Status(String name, String description, boolean isGood) {
//		this.id = 1;
		this.name = name;
		this.description = description;
		this.isGood = isGood;
	}
}