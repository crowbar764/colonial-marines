package lighting;

import java.awt.Color;
import java.util.ArrayList;

import entity.Entity;
import entity.item.Light;
import entity.turf.Turf;
import game.DEFINES;
import world.World;

public class SSLighting {
	
	public static Color[][] lightmap;
	public static boolean needsUpdate;
	public static Color baseLight;
	
	public static void init() {
		lightmap = new Color[World.width][World.height];
		needsUpdate = false;
		baseLight = new Color(0, 0, 0, 225);
		
		update();
	}
	
	public static void update() {
		
		// Set base map lighting
		for(int y = 0; y < World.height; y++) {
			for(int x = 0; x < World.width; x++) {
				lightmap[x][y] = baseLight;
			}
		}
		
		// For every tile on map
		for(int y = 0; y < World.height; y++) {
			for(int x = 0; x < World.width; x++) {
				
				Turf turf = World.getTurf(x, y);
				ArrayList<Entity> occupants = turf.getOccupants();
				
				// For each tile occupant
				for(Entity occupant: occupants) {
					
					// If occupant is light
					if(occupant.entityType == DEFINES.ITEM) {
						Light light = (Light)occupant;
						
						// For each tile in light's range
						for(int yy = -light.range; yy < light.range + 1; yy++) {
							System.out.println("dd");
							for(int xx = -light.range; xx < light.range + 1; xx++) {
								
								
								// If tile is in map bounds
								if(x + xx > -1 && x + xx < World.width && y + yy > -1 && y + yy< World.height) {
									
//									System.out.println(World.getTurf(x + xx, y + yy).type);
//									System.out.println(World.getTurf(x + xx, y + yy).type);
									if(World.getTurf(x + xx, y + yy).type == DEFINES.HULL) { // 
//										return;
										lightmap[x + xx][y + yy] = new Color(0, 0, 0, 0);
										continue;
									}
									
//									System.out.println("ddd");
//									for(int yyy = -light.range; yyy < light.range + 1; yyy++) {
//										for(int xxx = -light.range; xxx < light.range + 1; xxx++) {
//											
//										}
//									}
									
									double distance = (int)((Math.abs(xx) + Math.abs(yy)));
									Color color = lightmap[x + xx][y + yy];
									int r = color.getRed() + (int)(light.colour.getRed() / distance);
									r = sanitiseColor(r);
									int g = color.getGreen()+ (int)(light.colour.getGreen() / distance);
									g = sanitiseColor(g);
									int b = color.getBlue()+ (int)(light.colour.getBlue() / distance);
									b = sanitiseColor(b);
									
									double xa = 10.75;
									double xb = 1.55;
									double xc = light.colour.getAlpha();
									double attenuation = (xc - (xa * distance) - (xb * (Math.pow(distance, 2)))) * -1;
									int a = sanitiseColor((int)attenuation);
									if(a > color.getAlpha()) {
										a = color.getAlpha();
									}
									
									lightmap[x + xx][y + yy] = new Color(r, g, b, a);
								}
							}
						}
					}
				}
				

				
			}
		}
		needsUpdate = false;
	}
	
	private static int sanitiseColor(int value) {
		if(value > 255) {
			value = 255;
		} else if(value < 0) {
			value = 0;
		}
		return value;
	}
}