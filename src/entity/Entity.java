package entity;

import audio.SSAudio;
import entity.mob.Mob;
import game.DEFINES;
import world.World;

public class Entity {
	
	public final int entityType;
	public int entityID, state, animStep, hitPoints, faction, x, y, range;
	public static int lastID;
	public boolean isSelected, isSelectable, isCollidable;
	protected String killAudio;

	public Entity(int entityType, int faction) {
		
		this.entityType = entityType;
		lastID++;
		entityID = lastID * -1;
		isSelectable = true;
		isCollidable = true;
		hitPoints = -1;
		this.faction = faction;
		range = 1;
	}
	
	public void kill() {
		System.out.println(this + " dead!");
		
		switch (entityType) {
		case DEFINES.MOB:
			SSAudio.newSound("effects/" + killAudio);
			World.removeMob((Mob)this);
			break;
		default:
			System.out.println("ERROR: Deletion method not set for: " + this);
			break;
		}
	}
	
	public void process() {
		
	}
	
	public void fastProcess() {
		
	}
}