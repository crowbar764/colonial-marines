package entity.turf;

import java.awt.Point;

import game.DEFINES;

public class Plating extends Turf {

	public Plating(Point position) {
		super(DEFINES.PLATING, position);
		
		oxygen = 0;
		newOxygen = oxygen;
		isOpen = true;
	}
}