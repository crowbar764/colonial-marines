package entity.turf;

import java.awt.Point;

import game.DEFINES;

public class Vent extends Turf {

	public Vent(Point position) {
		super(DEFINES.VENT, position);
		
		oxygen = 100;
		newOxygen = oxygen;
		isOpen = true;
	}
}