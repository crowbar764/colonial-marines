package entity.turf;

import java.awt.Point;
import java.util.ArrayList;

import entity.Entity;
import game.DEFINES;

public class Turf extends Entity {
	
	public int type;
	public boolean isOpen;
	public int oxygen;
	public int newOxygen;
	private ArrayList<Entity> occupants;

	public Turf(int type, Point position) {
		
		super(DEFINES.TURF, DEFINES.WORLD);
		this.type = type;
		entityID = entityID - ((DEFINES.TURF + 1) * 1000000);
		occupants = new ArrayList<Entity>();
		x = position.x;
		y = position.y;
	}
	
	public void process() {
		
		//oxygen = 5;
	}
	
	public ArrayList<Entity> getOccupants() {
		return occupants;
	}
	
	public void addOccupant(Entity entity) {
		occupants.add(entity);
	}
	
	public void removeOccupant(Entity entity) {
		occupants.remove(entity);
	}
}