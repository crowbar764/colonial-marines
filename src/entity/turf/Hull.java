package entity.turf;

import java.awt.Point;

import game.DEFINES;

public class Hull extends Turf {

	public Hull(Point position) {
		super(DEFINES.HULL, position);
		
		oxygen = 0;
		newOxygen = oxygen;
		isOpen = false;
	}
}