package entity.item;

import java.awt.Color;

import game.DEFINES;

public class Light extends Item {
	public Color colour = new Color(0, 0, 0, 50);
	public int range = 7;

	public Light(int x, int y) {
		super(x, y);
		itemType = DEFINES.LIGHT;
	}
}