package entity.item;

import entity.Entity;
import game.DEFINES;
import world.World;

public class Item extends Entity {
	
	public int itemType;

	public Item(int x, int y) {
		super(DEFINES.ITEM, DEFINES.WORLD);
		this.x = x;
		this.y = y;
		entityID = entityID - ((DEFINES.ITEM + 1) * 1000000);
		state = animStep = 0;
		World.getTurf(World.toGrid(x), World.toGrid(y)).addOccupant(this);
		World.addItem(this);
	}
	
	public void process() {

		if (animStep == 0) {
			animStep++;
		} else {
			animStep = 0;
		}
	}
}