package entity.effect;

import java.awt.Rectangle;

import entity.Entity;
import game.DEFINES;
import world.World;

public class Effect extends Entity {

	public float X, Y;
	public int width, height; //For collision rotx/y calculation. Not tested out of standard 32.
	public double heading, speed;
	public Rectangle collisionBox;
	
	public Effect(int x, int y, double heading, int faction) {
		
		super(DEFINES.EFFECT, faction);
		entityID = entityID - ((DEFINES.EFFECT + 1) * 1000000);
		isSelectable = false;
		X = x;
		Y = y;
		width = height = 32;
		this.heading = heading;
		collisionBox = new Rectangle(0, 0, 32, 32);
		this.faction = faction;
		
		World.effects.add(this);
	}
	
	public void process() {
	}
	
	public void fastProcess() {
	}
}