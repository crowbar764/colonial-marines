package entity.effect;

import java.awt.Rectangle;

import audio.SSAudio;
import entity.Entity;
import game.DEFINES;
import world.World;

public class Projectile extends Effect {
	
	int damage;

	public Projectile(int x, int y, double heading, int faction) {
		super(x, y, heading, faction);
		speed = 1;
	}
	
	public void process() {

	}
	
	public void fastProcess() {
		
		X += speed * Math.cos(Math.toRadians(heading));
		Y += speed * Math.sin(Math.toRadians(heading));
		
		collisionCheck();
	}
	
	public void collisionCheck() {
		Entity collision = World.checkForCollision(new Rectangle(
				Math.round(X), 
				Math.round(Y),
				collisionBox.width,
				collisionBox.height),
				width,
				height,
				collisionBox.x,
				collisionBox.y,
				Math.toRadians(heading));

		if(collision != null) {
			onCollide(collision);
		}
	}
	
	public void onCollide(Entity obstruction) {
		
		//Friendly fire (with exception of World (0))
		if(obstruction.faction == faction && faction != 0) {
			return;
		}
		
		// Can be damaged
		if(obstruction.hitPoints != -1) {
			obstruction.hitPoints = obstruction.hitPoints - damage;
			
			switch (obstruction.entityType) {
			case DEFINES.TURF:
//				new SimpleAudioPlayer("assets/audio/effects/impact.wav");
				break;
			case DEFINES.MOB:
				SSAudio.newSound("effects/impact");
				break;
			default:
				System.out.println("ERROR: Impact sound not set for type: " + obstruction.entityType);
				break;
			}	
			
			if(obstruction.hitPoints <= 0) {
				obstruction.kill();
			}
		}
		
		World.removeEffect(this);
	}
}