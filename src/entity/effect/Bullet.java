package entity.effect;

import java.awt.Rectangle;

import audio.SSAudio;

public class Bullet extends Projectile {

	public Bullet(int x, int y, double heading, int faction) {
		super(x, y, heading, faction);
		collisionBox = new Rectangle(28, 14, 4, 4);
		damage = 10;
		speed = 8;
	}
	
//	public void process() {
//	}
	
//	public void fastProcess() {
//		this.super();
//	}
}