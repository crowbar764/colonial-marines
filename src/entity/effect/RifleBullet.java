package entity.effect;

import audio.SSAudio;

public class RifleBullet extends Bullet {
	public RifleBullet(int x, int y, double heading, int faction) {
		super(x, y, heading, faction);
		damage = 5;
		speed = 8;
		SSAudio.newSound("effects/m4a1");
	}
}
