package entity.effect;

import audio.SSAudio;

public class SniperBullet extends Bullet {

	public SniperBullet(int x, int y, double heading, int faction) {
		super(x, y, heading, faction);
		damage = 10;
		speed = 8;
		SSAudio.newSound("effects/sniper");
	}
}