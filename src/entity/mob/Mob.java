package entity.mob;

import java.awt.Point;
import java.util.ArrayList;

import ai.BehavourTree;
import entity.Entity;
import entity.effect.Bullet;
import entity.effect.Projectile;
import game.DEFINES;
import game.Game;
import math.AStar;
import math.Node;
import math.Path;
import status.SSStatus;
import world.World;

public class Mob extends Entity {

	public int mobType, targetsX, targetsY, magSize, currentMag;
	public float targetsRotation;
	public double targetsDirection;
	public long shotCooldown, lastShotTime, reloadTime, reloadStartTime;
	public Point targetLocation;
	public Path path;
	protected int pathIndex;
	public BehavourTree ai;
	public Entity target;
	public boolean[] statuses;
	public Projectile munnition;

	public Mob(int type, int x, int y, int faction) {

		super(DEFINES.MOB, faction);
		this.mobType = type;
//		System.out.println(entityID);
		entityID = entityID - ((DEFINES.MOB + 1) * 1000000);
		this.x = x;
		this.y = y;
		World.getTurf(World.toGrid(x), World.toGrid(y)).addOccupant(this);
		state = animStep = 0;
		ai = new BehavourTree(this);
		targetLocation = null;
		lastShotTime = System.nanoTime();
		path = new Path(new ArrayList<Node>());
		hitPoints = 100;
		statuses = new boolean[SSStatus.statuses.size()];
		statuses[0] = true; //DEBUG
//		statuses[1] = true; //DEBUG
		statuses[2] = true; //DEBUG
		World.addMob(this);
	}
	
	public void shoot() {
		
	}

	public void process() {

		// for(Mob mob : World.mobs) {
		// System.out.println(mob.type);
		// if(mob.type == DEFINES.ENGINEER) {
		// }
		// }

		processMove();

		if (animStep == 0) {
			animStep++;
		} else {
			animStep = 0;
		}
	}

	public void processMove() {

		if (targetLocation == null) {
			return;
		}

		Point newPosition = path.nodes.get(pathIndex).position;
		World.getTurf(World.toGrid(x), World.toGrid(y)).removeOccupant(this);
		x = World.toPixels(newPosition.x);
		y = World.toPixels(newPosition.y);
		World.getTurf(World.toGrid(x), World.toGrid(y)).addOccupant(this);
		path.nodes.remove(pathIndex);
		pathIndex--;
		

		if (pathIndex == -1) { // -1 to reach end
			targetLocation = null;
		}
	}

	public void setTargetLocation(int x, int y) {
		
		targetLocation = new Point(x, y);
		Point position = new Point(World.toGrid(this.x), World.toGrid(this.y));

		path.nodes = AStar.findPath(position, targetLocation);

		if (path.nodes == null) {
			System.out.println("WARNING: Position unreachable, aborting.");
			targetLocation = null;
			AStar.nodes = null;
			return;
		}

		pathIndex = path.nodes.size() - 1;
	}
}