package entity.mob.player;

import ai.BT_Marine;
import entity.effect.Bullet;
import entity.effect.Projectile;
import entity.effect.RifleBullet;
import game.DEFINES;

public class Marine extends Human {

	public Marine(int x, int y) {
		super(DEFINES.MARINE, x, y);
		shotCooldown = 75000000l;
		reloadTime = 800000000l;
		magSize = 5;
		currentMag = magSize;
		
//		Object dmunnition = Bullet.class;
//		Class<Bullet> a = Bullet;
//		System.out.println((Bullet)dmunnition);
		ai = new BT_Marine(this);
	}

//	public void fastProcess() {
//		super.fastProcess();
//	}
	
	public void shoot() {
//		super.shoot();
		new RifleBullet(x, y, targetsDirection, faction);
	}
}