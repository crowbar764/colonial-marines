package entity.mob.player;

import entity.mob.Mob;
import game.DEFINES;

public class Human extends Mob {

	public Human(int type, int x, int y) {
		super(type, x, y, DEFINES.PLAYER);
		range = 7;
		killAudio = "death";
	}
}