package entity.mob.player;

import ai.BT_Marksman;
import entity.effect.SniperBullet;
import game.DEFINES;

public class Marksman extends Human {
	public Marksman(int x, int y) {
		super(DEFINES.MARKSMAN, x, y);
		range = 9;
		shotCooldown = 1000000000l;
		ai = new BT_Marksman(this);
	}
	
	public void fastProcess() {
//		super.fastProcess();
//		
//		if(ai.status == DEFINES.DEFENDING) {
//			
//			if(target.hitPoints < 1) {
//				target = null;
//				ai.status = DEFINES.IDLE;
//			} else {
//				targetsX = target.x;
//				targetsY = target.y;
//				shoot();
//			}
//		}
	}
	
	public void shoot() {
		new SniperBullet(x, y, targetsDirection, faction);
	}
	
//	public void fastProcess() {
////		super.fastProcess();
////		
////		if(ai.status == DEFINES.DEFENDING) {
////			
////			if(target.hitPoints < 1) {
////				target = null;
////				ai.status = DEFINES.IDLE;
////			} else {
////				targetsX = target.x;
////				targetsY = target.y;
////				shoot();
////			}
////		}
//	}
}