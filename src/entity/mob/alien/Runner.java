package entity.mob.alien;

import game.DEFINES;

public class Runner extends Alien {

	public Runner(int x, int y) {
		super(DEFINES.RUNNER, x, y);
	}
}