package entity.mob.alien;

import entity.mob.Mob;
import game.DEFINES;

public class Alien extends Mob {
	
	public Alien(int type, int x, int y) {
		super(type, x, y, DEFINES.XENOS);
//		isSelectable = false;
		killAudio = "death";
	}
}