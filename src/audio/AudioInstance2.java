package audio;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

import audio.audiocue.AudioCue;
import audio.audiocue.AudioCueInstanceEvent;
import audio.audiocue.AudioCueListener;

public class AudioInstance2 { 

	public AudioInstance2() throws InterruptedException, UnsupportedAudioFileException, IOException, IllegalStateException, LineUnavailableException {
		// Simple case example ("fire-and-forget" playback):
		// assumes sound file "myAudio.wav" exists in same file folder,
		// we will allow up to four concurrent instances.

		// Preparatory steps (do these prior to playback): 
		URL url = this.getClass().getResource("res/m4a1.wav");
		AudioCue myAudioCue = AudioCue.makeStereoCue(url, 4); //allows 4 concurrent
		myAudioCue.open();  // see API for parameters to override "sound thread" configuration defaults

		// For playback, normally done on demand:
		myAudioCue.play(1, 0, 0.25, 0);  // see API for parameters to override default vol, pan, pitch 

		// release resources when sound is no longer needed
//		myAudioCue.close();
	}
} 