package audio;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.SourceDataLine;
import javax.sound.sampled.UnsupportedAudioFileException;

public class AudioInstance3 {

    public void playSfx(final InputStream fileStream) {
        ActivityManager.getInstance().submit(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedInputStream bufferedStream = new BufferedInputStream(fileStream);
                    AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(bufferedStream);

                    final int BUFFER_SIZE = 128000;
                    SourceDataLine sourceLine = null;

                    AudioFormat audioFormat = audioInputStream.getFormat();
                    DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);


                    sourceLine = (SourceDataLine) AudioSystem.getLine(info);
                    

                    
                    sourceLine.open(audioFormat);

                    if (sourceLine == null) {
                        return;
                    }

                    sourceLine.start();
                    int nBytesRead = 0;
                    byte[] abData = new byte[BUFFER_SIZE];
                    while (nBytesRead != -1) {
                        try {
                            nBytesRead = bufferedStream.read(abData, 0, abData.length);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        if (nBytesRead >= 0) {
                            sourceLine.write(abData, 0, nBytesRead);
                        }
                    }

                    sourceLine.drain();
                    sourceLine.close();
                    bufferedStream.close();
                    audioInputStream.close();

                } catch (IOException e) {
                    e.printStackTrace();
                } catch (UnsupportedAudioFileException e) {
                    e.printStackTrace();
                } catch (LineUnavailableException e) {
                    e.printStackTrace();
                    System.exit(1);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        });
    }
}
