package audio;

import java.io.File; 

import javax.sound.sampled.AudioInputStream; 
import javax.sound.sampled.AudioSystem; 
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.LineEvent;

public class AudioInstance { 

	Long currentFrame; 
	Clip clip; 
	String status; 
	AudioInputStream audioInputStream;
	int x, y;

	public AudioInstance(String file, float volume) {
		x = 1;
		y = 1;
		
		try {
			audioInputStream = AudioSystem.getAudioInputStream(new File("assets/audio/" + file + ".wav").getAbsoluteFile()); 
			clip = AudioSystem.getClip(); 
			clip.open(audioInputStream);
		} catch (Exception e) {
			System.out.println("ERROR: Audio malfunction");
			e.printStackTrace();
		}
		
		((FloatControl)clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(volume);
		
		clip.addLineListener(e -> {
			if (e.getType() == LineEvent.Type.STOP) {
				SSAudio.removeInstance(this);
		    }
		});
		
		clip.start();
		status = "play";
	} 
	
	public void stop() { 
		currentFrame = 0L; 
		clip.stop(); 
		clip.close(); 
	} 
} 