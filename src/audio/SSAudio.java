package audio;

import java.util.ArrayList;
import java.util.Random;

import javax.sound.sampled.FloatControl;

import world.Camera;

public class SSAudio {
	
	private static ArrayList<AudioInstance> instances;

	public static void newSound(String file) {
		
		Random rand = new Random();
		float randVolume = rand.nextFloat() * -5;
		AudioInstance instance = new AudioInstance(file, randVolume);
		
		instances.add(instance);
	}
	
	public static void removeInstance(AudioInstance instance) {
		instances.remove(instance);
	}
	
	public static void process() {
		for(AudioInstance instance : instances) {
			((FloatControl)instance.clip.getControl(FloatControl.Type.MASTER_GAIN)).setValue(-(Camera.x / 32));
		}
	}
	
	public static void init() {
		instances = new ArrayList<AudioInstance>();
	}
}