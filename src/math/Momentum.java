package math;

public class Momentum {
	
	public int velocityX, velocityY, velocityZ, minZ, maxZ;
	
	public Momentum() {
//		velocityX = 0velocityY + velocityZ = 0;
	}
	
	public void addMomentum(int x, int y) {
		velocityX += x;
		velocityY += y;
		
		// Clamp horizontal speed.
		if(velocityX > 10) {
			velocityX = 10;
		} else if(velocityX < -10) {
			velocityX = -10;
		}
		
		// Clamp vertical speed.
		if(velocityY > 10) {
			velocityY = 10;
		} else if(velocityY < -10) {
			velocityY = -10;
		}
	}
	
	public void addMomentum(int x, int y, double z) {
		addMomentum(x, y);
		velocityZ += (z * 5);
		
		// Clamp Z speed.
		if(velocityZ > 30) {
			velocityZ = 30;
		} else if(velocityZ < -30) {
			velocityZ = -30;
		}
	}
	
	public void process() {
		// Bleed horizontal speed.
		if(velocityX > 0) {
			velocityX--;
		} else if(velocityX < 0) {
			velocityX++;
		}
		
		// Bleed vertical speed.
		if(velocityY > 0) {
			velocityY--;
		} else if(velocityY < 0) {
			velocityY++;
		}
		
//		 Bleed Z speed.
		if(velocityZ > 0) {
			velocityZ--;
		} else if(velocityZ < 0) {
			velocityZ++;
		}
	}
}
