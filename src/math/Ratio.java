package math;

public class Ratio {
	
	public int iWidth, iHeight;
	public double width, height;
	
	public Ratio(Double width, Double height) {
		this.width = width;
		this.height = height;
		iWidth = (int)Math.ceil(width);
		iHeight = (int)Math.ceil(height);
	}
}