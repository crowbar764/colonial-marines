package math;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import entity.turf.Turf;
import world.World;

public class AStar {

	static Turf[][] turfs;
	public static Node[][] nodes;
	static Point start, end;
	static Node currentNode;
	public static ArrayList<Node> open;
	public static ArrayList<Node> closed;
	private static ArrayList<Node> path;
	

	public static ArrayList<Node> findPath(Point s, Point e) {

		
		int width = World.width;
		int height = World.height;
		start = s;
		end = e;
		turfs = World.turfs;
		nodes = new Node[width][height];

		open = new ArrayList<Node>(0);
		closed = new ArrayList<Node>(0);

		nodes[start.x][start.y] = new Node(start.x, start.y, 0, null, new Color(100,100,255)); //START
		//nodes[end.x][end.y] = new Node(end.x, end.y, 0, null, new Color(255,100,100)); //END
		open.add(nodes[start.x][start.y]);

		do {
			if(getBestNode() == null) {
//				System.out.println("OPEN LIST EMPTY");
				break;
			}

			currentNode = getBestNode();
			closed.add(currentNode);
			open.remove(currentNode);

			if(closed.contains(nodes[end.x][end.y])) {
				break;
			}

			if(getAdjacents(currentNode.position.x, currentNode.position.y) == null) {
				continue;
			}
			ArrayList<Node> adjacentNodes = getAdjacents(currentNode.position.x, currentNode.position.y);

			for(Node aNode : adjacentNodes) {
				if(aNode == null) {
					continue;
				}

				if(closed.contains(nodes[aNode.position.x][aNode.position.y])) {
					continue;
				}

				if(!open.contains(aNode)) {

					aNode.parent = currentNode;
					open.add(aNode);
				} else {

					//if(aNode.a < currentNode.a + 1) {
					//	aNode.a = currentNode.a + 1;
					//}	
				}
			}
		} while(true);

		path = new ArrayList<Node>(0);
		Node breadcrumb = nodes[end.x][end.y];
		
		if (breadcrumb == null) { //If path isn't reachable?
			return null;
		}

		do  {	
			path.add(breadcrumb);
//			System.out.println(breadcrumb.position.x);

			if(breadcrumb.parent == null) {
				
				break;
			}
			breadcrumb = breadcrumb.parent;
		}while(true);
		
		return path;
	}

	public static Node getBestNode() {

		Node lowestNode = null;
		int lowestF = 999;

		for(Node node : open) {
			if(node.f < lowestF) {
				lowestNode = node;
				lowestF = node.f;
			} else if(node.f == lowestF) {
				lowestNode = node;
			}
		}
		return lowestNode;
	}

	public static int calculateB(int x, int y) {
		return Math.abs(end.x - x) + Math.abs(end.y - y);
	}

	private static ArrayList<Node> getAdjacents(int xPos, int yPos) {
		ArrayList<Node> adjacents = new ArrayList<Node>(0);

		if(checkValid(xPos + 1, yPos)) {
			adjacents.add(nodes[xPos + 1][yPos]);
		}

		if(checkValid(xPos, yPos + 1)) {
			adjacents.add(nodes[xPos][yPos + 1]);
		}

		if(checkValid(xPos - 1, yPos)) {
			adjacents.add(nodes[xPos - 1][yPos]);
		}

		if(checkValid(xPos, yPos - 1)) {
			adjacents.add(nodes[xPos][yPos - 1]);
		}
		return adjacents;
	}

	private static boolean checkValid(int x, int y) {
		Color colour = new Color(0,100,100);

		if(turfs[x][y].isOpen) {
			if(nodes[x][y] == null) {

				nodes[x][y] = new Node(x, y, currentNode.a + 1, currentNode, colour);
				return true;
			} else {
				return true;
			}
		}
		return false;
	}
}