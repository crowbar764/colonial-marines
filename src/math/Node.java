package math;

import java.awt.Color;
import java.awt.Point;

public class Node {

	public Point position;
	public int a, b, f;
	public Color colour;
	public Node parent;
	
	public Node(int x, int y, int a, Node parent, Color colour) {
		
		position = new Point(x, y);
		this.a = a;
		this.colour = colour;
		b = AStar.calculateB(x, y);//calculateA(nodes[xPos + 1][yPos]), calculateB(nodes[xPos + 1][yPos]);
		f = a + b;
		this.parent = parent;
		
		//World.markers.add(new Marker(x, y, colour, a, b, f, parent));
	}
	
	public void updateValues(int a, int b) {
		this.a = a;
		this.b = b;
		f = a + b;
		//World.markers.add(new Marker(x, y, colour, a, b, f, parent));
	}
}