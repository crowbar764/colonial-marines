package world;

import game.Game;
import math.Momentum;
import math.Ratio;
import render.Renderer;

public class Camera  {

	public static Ratio visibleResolution;
	public static int x, y;
	public static Momentum momentum;
	
	public static void setPosition(int newX, int newY) {
		// AABB, it really is, just including zoom too.
		
		if(newX < 0) {
			newX = 0;
		} else if (visibleResolution.width + newX >= World.pixelResolution.width) {
			newX = (int)(World.pixelResolution.width - visibleResolution.width);
		}
		
		if(newY < 0) {
			newY = 0;
		} else if (visibleResolution.height + newY >= World.pixelResolution.height) {
			newY = (int)(World.pixelResolution.height - visibleResolution.height);
		}
		
		x = newX;
		y = newY;
	}
	
	public static void changeZoom(double zVelocity) {
		
		double newZoom = Renderer.zoom + (zVelocity / ((200 * (1 - normalise(Renderer.zoom, 8, 1))) + 1));
		
		if (newZoom > 6) {
			newZoom = 6;
		} else if (newZoom < 1) {
			newZoom = 1;
		}
		
		Renderer.zoom = newZoom;
		Ratio newVisibleResolution = new Ratio(Game.resX / Renderer.zoom, Game.resY / Renderer.zoom);
		
		// WIP zoom centering (stuttery)
//		if(zVelocity > 0) {
//			x = (int)(x + (visibleResolution.width - newVisibleResolution.width) / 2);
//			y = (int)(y + (visibleResolution.height - newVisibleResolution.height) / 2);
//		} else if(zVelocity < 0) { // Buggy and errors on out of bounds
//			x = (int)(x - (newVisibleResolution.width - visibleResolution.width) / 2);
//			y = (int)(y - (newVisibleResolution.height - visibleResolution.height) / 2);
//		}
		
		visibleResolution = newVisibleResolution;

		//TODO If ratio is larger than map
		//TODO for -x and -y too?
		
		//If camera position, when enacted, would escape the map X
		if(visibleResolution.width + x > World.pixelResolution.width) {
			x = (int)(World.pixelResolution.width - visibleResolution.width);
		}
		
		//If camera position, when enacted, would escape the map Y
		if(visibleResolution.height + y > World.pixelResolution.height) {
			y = (int)(World.pixelResolution.height- visibleResolution.height);
		}
		
		Game.renderer.updateZoom();
	}
	
	public static void process() {
		momentum.process();
		
		if(momentum.velocityX != 0 || momentum.velocityY != 0) {
			setPosition(x + momentum.velocityX, y + momentum.velocityY);
		}
		
		if(momentum.velocityZ != 0) {
			changeZoom(momentum.velocityZ);
		}
	}
	
	public static double normalise(double val, double max, double  min) {
		return (val - min) / (max - min);
	}
	
	public static void init() {
		momentum = new Momentum();
//		visibleResolution = new Ratio(Game.resX / Renderer.zoom, Game.resY / Renderer.zoom);
	}
}