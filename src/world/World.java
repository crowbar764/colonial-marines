package world;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entity.Entity;
import entity.effect.Effect;
import entity.item.Item;
import entity.item.Light;
import entity.mob.Mob;
import entity.mob.alien.Runner;
import entity.mob.player.Marine;
import entity.mob.player.Marksman;
import entity.turf.Hull;
import entity.turf.Plating;
import entity.turf.Space;
import entity.turf.Turf;
import entity.turf.Vent;
import game.DEFINES;
import game.Game;
import render.Renderer;
import render.ResourceManager;

public class World {

	public static int width;
	public static int height;
	public static Dimension pixelResolution;
	public static Turf[][] turfs;
	public static ArrayList<Mob> mobs;
	public static ArrayList<Effect> effects;
	public static ArrayList<Item> items;
	public static ArrayList<Entity> lightSources;
	//public static ArrayList<Marker> markers;
	
	private static Entity collisionCheck(int x, int y, int rotX, int rotY, double rotation) {
		
		double xTranslated = x - rotX;
		double yTranslated = y - rotY;
		
		double xRotated = xTranslated * Math.cos(rotation) - yTranslated * Math.sin(rotation);
		double yRotated = xTranslated * Math.sin(rotation) + yTranslated * Math.cos(rotation);
		
		double xResult = xRotated + rotX;
		double yResult = yRotated + rotY;
		
		Turf turf = turfs[toGrid((int)xResult)][toGrid((int)yResult)];
		
		//If turf is solid (collidable wall)
		if(!turf.isOpen) {
			return turf;
		} else { // Check turf's entities for collidables
			ArrayList<Entity> occupants = turf.getOccupants();
			
			for(Entity occupant: occupants) {
				if(occupant.isCollidable) {
					return occupant;
				}	
			}
		}
		
		//Nothing is collidable
		return null;
	}
	
	public static Entity checkForCollision(Rectangle collider, int spriteWidth, int spriteHeight, int offsetX, int offsetY, double rotation) {
		
		Entity entity;
		int x = collider.x + offsetX;
		int y = collider.y + offsetY;
		int rotX = collider.x + (spriteWidth / 2);
		int rotY = collider.y + (spriteHeight / 2);
		
		entity = collisionCheck(x, y, rotX, rotY, rotation);
		
		if(entity != null) {
			return entity;
		}
		
		entity = collisionCheck(x + collider.width, y, rotX, rotY, rotation);
		
		if(entity != null) {
			return entity;
		}
		
		entity = collisionCheck(x, y + collider.height, rotX, rotY, rotation);
		
		if(entity != null) {
			return entity;
		}
		
		entity = collisionCheck(x + collider.width, y + collider.height, rotX, rotY, rotation);
		
		if(entity != null) {
			return entity;
		}
		
		return null;
	}

	public static void loadMap(String name) {

		BufferedImage mapImage = ResourceManager.loadImage(name);
		turfs = new Turf[mapImage.getWidth()][mapImage.getHeight()];
		//turfs = new ArrayList<Turf>(mapImage.getWidth() * mapImage.getHeight());
		mobs = new ArrayList<Mob>();
		effects = new ArrayList<Effect>();
		items = new ArrayList<Item>();
		lightSources = new ArrayList<Entity>();
		//markers = new ArrayList<Marker>(0);
		//int currentTile = 0;
		//int totalTiles = mapImage.getWidth() * mapImage.getHeight();
		//String percentage;
		//DecimalFormat df = new DecimalFormat("####0.00");

		for(int y = 0; y < mapImage.getHeight(); y++) {
			for(int x = 0; x < mapImage.getWidth(); x++) {

				switch (mapImage.getRGB(x, y)) {
				case DEFINES._SPACE:
					turfs[x][y] = new Space(new Point(x, y));
					break;
				case DEFINES._HULL:
					turfs[x][y] = new Hull(new Point(x, y));
					break;
				case DEFINES._PLATING:
					turfs[x][y] = new Plating(new Point(x, y));
					break;
				case DEFINES._VENT:
					turfs[x][y] = new Vent(new Point(x, y));
					break;
				default:
					System.out.println("ERROR: MAP TILE NOT FOUND: " + mapImage.getRGB(x, y));
					break;
				}
				//currentTile++;
				//percentage = df.format(((float)currentTile / (float)totalTiles) * 100);
				//System.out.println("Loading map: " + percentage + "%");
			}
		}
		
		width = mapImage.getWidth();
		height = mapImage.getHeight();
		pixelResolution = new Dimension(width * Renderer.pixelRes, height * Renderer.pixelRes);
		
		new Marine((int)(5 * Game.pixelRes), (int)(1 * Game.pixelRes));
//		new Marine((int)(6 * Game.pixelRes), (int)(3 * Game.pixelRes));
		new Marksman((int)(11 * Game.pixelRes), (int)(4 * Game.pixelRes));
		new Runner((int)(10 * Game.pixelRes), (int)(7 * Game.pixelRes));
//		new Light((int)(8 * Game.pixelRes), (int)(6 * Game.pixelRes));
		new Light((int)(10 * Game.pixelRes), (int)(10 * Game.pixelRes));
	}

	public static Turf getTurf(int x, int y) {

		//for(int column = 0; column < World.height; column++) {
		//	for(int row = 0; row < World.width; row++) {
		//		if(turf.position.x == position.x && turf.position.y == position.y) {
		//			return turf;
		//	}
		//}
		try {
			return turfs[x][y];
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("WARNING: Reaching entity outside of playzone");
			return turfs[x][y];
		}
	}

	public static int toGrid(int pixel) {
//		return (int)(pixel / (Game.pixelRes * Renderer.xScale));
		return (int)(pixel / Game.pixelRes);
	}
	
	public static int toPixels(double gridX) {
//		return (int)(gridX * Game.pixelRes * Renderer.xScale);
		return (int)(gridX * Game.pixelRes);
	}
	
	/*
	public static void setTurf(Point position, Turf newTurf) {

		for(Turf turf : turfs) {
			if(turf.position == position) {
				turf = newTurf;
			}
		}
		System.out.println("TURF AT POSITION " + position + " NOT FOUND.");
	}*/

	//public static int removeMob(int x, int y) {

	//for(Mob mob : mobList) {


	//}
	//return mobList[x][y];
	//}

	public static void addMob(Mob mob) {
		mobs.add(mob);
	}
	
	public static void addItem(Item item) {
		items.add(item);
	}
	
	public static void removeMob(Mob mob) {
		Turf parentTurf = World.getTurf(World.toGrid(mob.x), World.toGrid(mob.y));
		parentTurf.removeOccupant(mob);
		mobs.remove(mob); //Does this actually delete?
	}
	
	public static void removeEffect(Effect effect) {
		effects.remove(effect);
		effect = null; //Does this actually delete?
	}
	
	public static Entity getEntity(int ID) {
		
		int identifyer = ID * -1;
	    while (identifyer > 9) {
	    	identifyer /= 10;
	    }
	    
		switch (identifyer) {
		
		case DEFINES.TURF + 1:
			for(Turf[] turfColumn : turfs) {
				for(Turf turf : turfColumn) {
					if(turf.entityID == ID) {
						return turf;
					}
				}
			}
			break;
		case DEFINES.MOB + 1:
			for(Mob mob : mobs) {
				if(mob.entityID == ID) {
					return mob;
				}
			}
			break;
		case DEFINES.ITEM + 1:
			for(Item item : items) {
				if(item.entityID == ID) {
					return item;
				}
			}
			break;
		case DEFINES.EFFECT + 1:
			for(Effect effect : effects) {
				if(effect.entityID == ID) {
					return effect;
				}
			}
			break;
		default:
			System.out.println("ERROR: No case for entity type in World.getEntity()");
		}
		return null;
	}
}