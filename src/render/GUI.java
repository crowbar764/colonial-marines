package render;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

import audio.SSAudio;
import entity.Entity;
import entity.mob.Mob;
import entity.turf.Hull;
import entity.turf.Plating;
import entity.turf.Space;
import entity.turf.Vent;
import game.DEFINES;
import game.Game;
import io.SSSelect;
import status.SSStatus;

public class GUI {
	
	public static JPanel selectionPanel, titlePanel, statusPanel, aboutPanel;
	public static JLabel selectionTypeLabel, selectionType, selectionID, selectionPanelTitleLabel;
	public static JCheckBoxMenuItem complexAICheck, clickMaskCheck;
	public static StatusListItem[] statusListItems;
	public static Font font1 = new Font("Helvetica", Font.PLAIN, 16);

	public static void init() {
		
		
		int GUIMargin = 20;
		int panel1BorderSize = 2;
		
		Dimension panel1Bounds = new Dimension (200, 300);
		selectionPanel = new JPanel(new GridBagLayout());
		GridBagConstraints selectionPanelC = new GridBagConstraints();
		selectionPanelC.fill = GridBagConstraints.HORIZONTAL; // Fill extra horizontal space (stretch).
		selectionPanelC.weightx = 1; // Fill extra horizontal space (stretch).
		selectionPanel.setBounds(GUIMargin, Game.resY - panel1Bounds.height - GUIMargin, panel1Bounds.width, panel1Bounds.height);
		selectionPanel.setBackground(DEFINES.BLUEGREYDARK);
		selectionPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK, panel1BorderSize));
		
		titlePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
		titlePanel.setBackground(DEFINES.BLUEGREYDARK);
		titlePanel.setBackground(DEFINES.BLUEGREY);
		selectionPanelTitleLabel = new JLabel();
		selectionPanelTitleLabel.setText("Marine");
		selectionPanelTitleLabel.setForeground(Color.WHITE);
		selectionPanelTitleLabel.setFont(font1);
		
		statusPanel = new JPanel(new GridBagLayout());
		GridBagConstraints statusPanelC = new GridBagConstraints();
		statusPanel.setBackground(DEFINES.BLUEGREY);
		statusPanel.setPreferredSize(new Dimension(0, 220));
		
		//////////////////
		// STATUS PANEL //
		//////////////////
		
		statusListItems = new StatusListItem[SSStatus.statuses.size()];
		for (int i = 0; i < SSStatus.statuses.size(); i++) {
			statusListItems[i] = new StatusListItem(SSStatus.statuses.get(i));
		}
		
		GridLayout layout3 = new GridLayout();
		aboutPanel = new JPanel(layout3);
		aboutPanel.setBackground(Color.GREEN);
		aboutPanel.setPreferredSize(new Dimension(0, 100));
		
		Button button1 = new Button();
		button1.setText("Status");
		button1.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				openMenu(0);
			}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {
				button1.setBackground(button1.hoverColour);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				button1.setBackground(button1.colour);
			}
		});
		
		Button button2 = new Button();
		button2.setText("About");
		button2.addMouseListener(new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {}

			@Override
			public void mousePressed(MouseEvent e) {
				openMenu(1);
			}

			@Override
			public void mouseReleased(MouseEvent e) {}

			@Override
			public void mouseEntered(MouseEvent e) {
				button2.setBackground(button2.hoverColour);
			}

			@Override
			public void mouseExited(MouseEvent e) {
				button2.setBackground(button2.colour);
			}
		});
		
		/////////////////////
		// SELECTION PANEL //
		/////////////////////
		
		titlePanel.add(selectionPanelTitleLabel);
		selectionPanelC.gridx = 0;
		selectionPanelC.gridy = 0;
		selectionPanelC.gridwidth = 2;
		selectionPanelC.insets = new Insets(0,0,15,0);
		selectionPanel.add(titlePanel, selectionPanelC);
		selectionPanelC.gridwidth = 1;
		selectionPanelC.gridx = 0;
		selectionPanelC.gridy = 1;
		selectionPanelC.insets = new Insets(0,0,0,1);
		selectionPanel.add(button1, selectionPanelC);
//		selectionPanel.add(new JSeparator(SwingConstants.VERTICAL));
		selectionPanelC.gridx = 1;
		selectionPanelC.gridy = 1;
		selectionPanelC.insets = new Insets(0,1,0,0);
		selectionPanel.add(button2, selectionPanelC);
		selectionPanelC.insets = new Insets(0,0,0,0);
		selectionPanelC.gridx = 0;
		selectionPanelC.gridy = 2;
		selectionPanelC.gridwidth = 2;
		selectionPanel.add(statusPanel, selectionPanelC);
		selectionPanel.add(aboutPanel, selectionPanelC);
		
		selectionPanel.setVisible(false);
	}
	
	private static void openMenu(int menu) {
		aboutPanel.setVisible(false);
		statusPanel.setVisible(false);
		
		switch(menu) {
		case 0:
			statusPanel.setVisible(true);
			break;
		case 1:
			aboutPanel.setVisible(true);
			break;
		}
	}
	
	public static void updateSelectionPanel() {
		
		ArrayList<Entity> selectedEntities = SSSelect.selectedEntities;
		
		if(selectedEntities.size() == 1) {
			selectionPanel.setVisible(true);
		} else {
			selectionPanel.setVisible(false);
			return;
		}
		
		Entity entity = selectedEntities.get(0);
		
		switch (entity.entityType) {
			case DEFINES.MOB:
				Mob mob = (Mob)entity;
				boolean[] entityStatuses = mob.statuses;
				
				GridBagConstraints statusPanelC = new GridBagConstraints();
				statusPanelC.gridx = 0;
				statusPanelC.gridwidth = 1;
				statusPanelC.fill = GridBagConstraints.HORIZONTAL; // Fill extra horizontal space (stretch).
				statusPanelC.weightx = 1; // Fill extra horizontal space (stretch).
				statusPanelC.insets = new Insets(0,5,0,5);  //top padding
//				statusPanelC.anchor = GridBagConstraints.PAGE_END;
//				statusPanelC.weighty = 1.0; 

//				int statusCount = 0;
				
				for (int i = 0; i < SSStatus.statuses.size(); i++) {
					statusPanel.remove(statusListItems[i]);

					if(entityStatuses[i]) {
//						statusPanelC.gridy = statusCount;
						statusPanel.add(statusListItems[i], statusPanelC);
//						statusCount++;
					}
				}
				
//				statusPanel.repaint();
				statusPanel.validate();
				
				break;
			default:
				System.out.println("No selection update set for type: " + entity.entityType);
				break;
		}
		
//		if(entity.)
		
		
		
		
		selectionPanelTitleLabel.setText(entity.getClass().getSimpleName());
		
		
		

		
		
		
//		
//		Map<Integer, String> map = new HashMap<>();
//		map.put(0, "Turf");
//		map.put(1, "Mob");
//		
//		if(selectedEntities.size() > 1) {
//			selectionType.setText("Many");
//			selectionID.setText("Many");
//		} else {
//			selectionType.setText(map.get(selectedEntities.get(0).entityType));
//			selectionID.setText(Integer.toString(selectedEntities.get(0).entityID));
//		}
		
	}
	
	public static void syncCheckboxes() {
		
		if(Renderer.renderComplexAI) {
//			complexAICheck.setSelected(true);
		}
		
		if(Renderer.renderClickMask) {
//			clickMaskCheck.setSelected(true);
		}
	}
	
	private static ItemListener toggleComplexAI = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == 1) {
				Renderer.renderComplexAI = true;
			} else {
				Renderer.renderComplexAI = false;
			}
		}
	};
	
	private static ItemListener toggleClickMask = new ItemListener() {
		@Override
		public void itemStateChanged(ItemEvent e) {
			if(e.getStateChange() == 1) {
				Renderer.renderClickMask = true;
			} else {
				Renderer.renderClickMask = false;
			}
		}
	};
}