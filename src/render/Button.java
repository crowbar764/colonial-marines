package render;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.Border;

public class Button extends JButton {
	private static final long serialVersionUID = 1L;
	public Color colour, hoverColour;
	public int age = 1;
	
	public Button() {
		colour = new Color(241, 196, 15);
		hoverColour = new Color(243, 156, 18);
		setBorderPainted(false);
		setFocusPainted(false); //Hide selection blue thing (ugly)
//		setContentAreaFilled(false);
		
		setBackground(colour);
//		Border raisedBorder = BorderFactory.createRaisedBevelBorder();
//		setBorder(raisedBorder);
	}
	
//    @Override
//    public void paintComponent(Graphics g) {
//
////        super.paintComponent(g);
////        doDrawing(g);
//    }
}