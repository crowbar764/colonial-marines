package render;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import game.DEFINES;
import status.Status;

public class StatusListItem extends JPanel {
	
	private static final long serialVersionUID = 1L;
	public JLabel statusEffect;
	
	public StatusListItem(Status status) {
//		Status s = (Status)status.getClass();
////		System.out.println((Status)status);
//		Class<? extends Status> statusf = status;
//		statusf.
//		String one = Rested.id;
		setLayout(new GridBagLayout());
		setBackground(DEFINES.BLUEGREY);
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL; // Fill extra horizontal space (stretch).
		c.weightx = 1; // Fill extra horizontal space (stretch).
		
		JLabel statusName = new JLabel();
		statusName.setText(status.name);
		statusName.setForeground(DEFINES.WHITE);
		add(statusName, c);
		
		statusEffect = new JLabel();
		statusEffect.setText(status.description);
		if(status.isGood) {
			statusEffect.setForeground(DEFINES.GREEN);
		} else {
			statusEffect.setForeground(DEFINES.RED);
		}
//		if(Integer.parseInt(statusEffect.getText()) < 0) {
//			statusEffect.setForeground(Color.RED);
//		}
		statusEffect.setHorizontalAlignment(SwingConstants.RIGHT);
		c.insets = new Insets(0,10,0,0);  //top padding
		c.fill = GridBagConstraints.NONE; // Fill extra horizontal space (stretch).
		c.weightx = 0; // Fill extra horizontal space (stretch).
		add(statusEffect, c);
		
//		statusName.setFont(font1);
//		this.setText("Name");
//		this.setForeground(Color.WHITE);
//		this.setFont(GUI.font1);
//		this.setOpaque(true);
//		this.setBackground(new Color(52, 73, 94));
		
		
		
		
//		this.addMouseListener(new MouseAdapter() {
//		    @Override
//		    public void mouseEntered(MouseEvent e) {
//		       	isHover();
//		       	System.out.println("1");
//		    }
//		    
//		    @Override
//		    public void mouseExited(MouseEvent e) {
//		       	isNotHover();
//		       	System.out.println("o");
//		    }
//		});
	}
	
//	public void set
	
//	private void isHover() {
//		
//		
//		this.setBackground(Color.red);
//	}
//	
//	private void isNotHover() {
//		this.setBackground(new Color(52, 73, 94));
//	}
}