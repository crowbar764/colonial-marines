package render;
import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import entity.Entity;
import entity.effect.Effect;
import entity.item.Item;
import entity.mob.Mob;
import entity.turf.Turf;
import game.DEFINES;
import game.Game;
import io.SSSelect;
import lighting.SSLighting;
import math.Node;
import math.Path;
import math.Ratio;
import world.Camera;
import world.World;

public class Renderer extends Canvas{

	private static final long serialVersionUID = 5300262837691172232L;
	private static BufferedImage buffer, maskBuffer;
	static Graphics2D graphics, maskGraphics;
	public static double zoom;
	public static int pixelRes;

	BufferStrategy strategy;

	public static boolean renderTurf, renderEffects, renderEntities, renderAtmosphere, renderSelection, renderAI, renderComplexAI, renderClickMask, renderCollision, renderLighting, renderComplexLighting;

	public void render() {

		graphics = (Graphics2D) strategy.getDrawGraphics();
		graphics.scale(zoom, zoom);
		
		if (renderTurf) {
			renderTurf();
		}
		
		if (renderAI) {
			renderAI();
		}
		
		if (renderEffects) {
			renderEffects();
		}

		if(renderSelection) {
			renderSelection();
		}
		
		if(renderClickMask) {
			graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, (float) 0.25)); //SRC_OVER to allow for transparent painting
			graphics.drawImage(maskBuffer, null, 0, 0);
		}
		
		graphics.dispose();
		strategy.show();
	}

	private void renderTurf() {
		
		//Four corners of visibility (sort of)
		int visibleTilePositionX = (int)Math.floor(Camera.x / Game.pixelRes);
		int visibleTilePositionY = (int)Math.floor(Camera.y / Game.pixelRes);
		int visibleTileResolutionX = (int)Math.ceil(Camera.visibleResolution.width / Game.pixelRes) + 1;
		int visibleTileResolutionY = (int)Math.ceil(Camera.visibleResolution.height / Game.pixelRes) + 1;
		
		//If excess render escapes map.
		if(visibleTilePositionX + visibleTileResolutionX > World.width) {
			visibleTileResolutionX = visibleTileResolutionX - 1;
		}
		
		if(visibleTilePositionY + visibleTileResolutionY > World.height) {
			visibleTileResolutionY = visibleTileResolutionY - 1;
		}
		
		for(int y = visibleTilePositionY; y < visibleTilePositionY + visibleTileResolutionY; y++) {
			for(int x = visibleTilePositionX; x < visibleTilePositionX + visibleTileResolutionX; x++) {
				Turf turf = World.getTurf(x, y);

				/*
				 * TURF
				 */
				if(renderTurf) {
					BufferedImage sprite = ResourceManager.getSprite(DEFINES.TURF, turf.type, 0, 0);
					drawSprite(sprite, (int)(x * Game.pixelRes) , (int)(y * Game.pixelRes) , 0, turf.entityID, turf.isSelected);
				}
				
				if (renderEntities) {
					renderEntities(turf.getOccupants());
				}

				/*
				 * ATMOS
				 */
//				if(renderAtmosphere && turf.oxygen > 0) {
//					//drawRectangle(worldLocation, new Point(1,1), (int)(turf.oxygen));
//				}	
				
				
				/*
				 * LIGHTING
				 */
				if (renderLighting) {
					if (renderComplexLighting) {
						graphics.setColor(Color.BLACK);
						graphics.drawString(Integer.toString(SSLighting.lightmap[x][y].getAlpha()), (int)(x * Game.pixelRes) - Camera.x + 10, (int)(y * Game.pixelRes) - Camera.y + 21);
					}
					graphics.setColor(SSLighting.lightmap[x][y]);
					
					graphics.fillRect(
							(int)(x * Game.pixelRes) - Camera.x,
							(int)(y * Game.pixelRes) - Camera.y,
							pixelRes,
							pixelRes
							);
				}
			}
		}
	}

	private void renderEntities(ArrayList<Entity> entities) {
		
		BufferedImage sprite;

		for(Entity entity : entities) {
			switch (entity.entityType) {
			case DEFINES.MOB:
				Mob mob = (Mob)entity;
				sprite = ResourceManager.getSprite(DEFINES.MOB, mob.mobType, mob.state, mob.animStep);
				drawSprite(sprite, mob.x, mob.y, Math.toRadians(0), mob.entityID, mob.isSelected);
				break;
			case DEFINES.ITEM:
				Item item = (Item)entity;
				sprite = ResourceManager.getSprite(DEFINES.ITEM, item.itemType, item.state, item.animStep);
				drawSprite(sprite, item.x, item.y, Math.toRadians(0), item.entityID, item.isSelected);
				break;
			default:
				System.out.println("entity render not supported yet");
				break;
			}
		}
	}
	
	private void renderEffects() {
		
		@SuppressWarnings("unchecked")
		ArrayList<Effect> effects = (ArrayList<Effect>) World.effects.clone();
		
		for(Effect effect : effects) {
			
			BufferedImage sprite = ResourceManager.getSprite(DEFINES.EFFECT, 0, 0, 0);
			
			drawSprite(sprite, (int)(effect.X), (int)(effect.Y), effect.heading, effect.entityID, effect.isSelected);
			
			if(renderCollision) {
				drawRectangle(
						(int)(effect.X),
						(int)(effect.Y),
						effect.collisionBox.x,
						effect.collisionBox.y,
						effect.collisionBox.width,
						effect.collisionBox.height,
						effect.heading,
						Color.RED
				);
			}
		}
	}

	private void drawSprite(BufferedImage sprite, int x, int y, double rotation, int entityID, boolean isSelected) {
		
		graphics.rotate(Math.toRadians(rotation), x + (pixelRes / 2) - Camera.x, y + (pixelRes / 2) - Camera.y);
		
		graphics.drawImage(
				sprite, 
				x - Camera.x,
				y - Camera.y,
				pixelRes,
				pixelRes,
				null
				);

		maskGraphics.setColor(new Color(entityID));
		maskGraphics.fillRect(
				x - Camera.x,
				y - Camera.y,
				pixelRes,
				pixelRes
				);
		
		
		
		if(isSelected) {
			BufferedImage selectSprite = ResourceManager.getSprite(DEFINES.EFFECT, 2, 0, 0);
			graphics.drawImage(
					selectSprite, 
					x - Camera.x,
					y - Camera.y,
					pixelRes,
					pixelRes,
					null
					);
		}
		
		graphics.rotate(-Math.toRadians(rotation), x + (pixelRes / 2) - Camera.x, y + (pixelRes / 2) - Camera.y);
		
	}
	
	private void drawRectangle(int objX, int objY, int colX, int colY, int colW, int colH, double rotation, Color colour) {

		graphics.setPaint(colour);
		
		graphics.rotate(Math.toRadians(rotation), objX + (pixelRes / 2) - Camera.x, objY + (pixelRes / 2) - Camera.y);
		
		graphics.fillRect(
				objX + colX - Camera.x,
				objY + colY - Camera.y,
				colW,
				colH
				);
		
		graphics.rotate(-Math.toRadians(rotation), objX + (pixelRes / 2) - Camera.x, objY + (pixelRes / 2) - Camera.y);
	}
	
	private void renderAI() {

		for(Mob mob : World.mobs) {
			if(mob.targetLocation == null || mob.path.nodes == null) {
				continue;
			}
			
			Path path = mob.path;
			
			for(Node node : path.nodes) {
				if(node == null) {
					continue;
				}
				
				if (renderComplexAI) {
					
					Font debugFont = new Font("Courier New", 1, 5);
					graphics.setFont(debugFont);
					
					int nodeX = (int)((node.position.x * pixelRes) - Camera.x);
					int nodeY = (int)((node.position.y * pixelRes) - Camera.y);
					graphics.setPaint(new Color(0, 0, 0));
	
					int sx = nodeX + 3;
					int sy = nodeY + 7;
					graphics.drawString("F:" + node.f, sx, sy);
					
					sx = nodeX + 16;
					sy = nodeY + 7;
					graphics.drawString("(" + node.position.x + "," + node.position.y + ")", sx, sy);
		
					sx = nodeX + 3;
					sy = nodeY + 29;
					graphics.drawString("A:" + node.a, sx, sy);
					
					sx = nodeX + 20;
					sy = nodeY + 29;
					graphics.drawString("B:" + node.b, sx, sy);
				}
	
				// Draw parent connectors
				if(node.parent != null) {
	
					graphics.setStroke(new BasicStroke(3));
					graphics.setColor(DEFINES.GREEN);
					graphics.drawLine(
							(int)((node.position.x * pixelRes) - Camera.x + 16),
							(int)((node.position.y * pixelRes) - Camera.y + 16),
							(int)((node.parent.position.x * pixelRes) - Camera.x + 16),
							(int)((node.parent.position.y * pixelRes) - Camera.y + 16)
							);	
				}
			}
		}
	}

	private void renderSelection() {
		
		graphics.setPaint(new Color(0, 102, 204, 25));
		Rectangle selection = SSSelect.selectArea;
		
		if (selection.width == 0 || selection.height == 0) {
			return;
		}
		
		graphics.fillRect(
				selection.x - Camera.x,
				selection.y - Camera.y,
				(int)selection.getWidth(),
				(int)selection.getHeight()
				);

		graphics.setPaint(new Color(51, 153,255, 255));
		graphics.setStroke(new BasicStroke(1));
		graphics.drawRect(
				selection.x - Camera.x,
				selection.y - Camera.y,
				(int)selection.getWidth(),
				(int)selection.getHeight()
				);
	}
	
	public void updateZoom() {
		maskBuffer = new BufferedImage((int)Math.ceil(Camera.visibleResolution.width), (int)Math.ceil(Camera.visibleResolution.height), BufferedImage.TYPE_INT_ARGB);
		maskGraphics = maskBuffer.createGraphics();
	}

	public void init() {

		pixelRes = Game.pixelRes;
		zoom = Game.startingZoom;
		buffer = new BufferedImage(100, 200, BufferedImage.TYPE_INT_ARGB);
		graphics = buffer.createGraphics();

		createBufferStrategy(2);
		strategy = getBufferStrategy();
		Camera.visibleResolution = new Ratio(Game.resX / Renderer.zoom, Game.resY / Renderer.zoom);
		updateZoom(); // Init maskbuffer
		
		renderTurf = true;
		renderEffects = true;
		renderEntities = true;
		renderAI = true;
//		renderComplexAI = true;
//		renderAtmosphere = true;
		renderSelection = true;
//		renderClickMask = true;
//		renderCollision = true;
		renderLighting = true;
//		renderComplexLighting = true;
	}
	
	public static BufferedImage getMask() {
		return maskBuffer;
	}
}