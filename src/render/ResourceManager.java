package render;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

import javax.imageio.ImageIO;

import game.DEFINES;
import game.Game;
import status.SSStatus;
import status.Status;

public class ResourceManager {

	private static File file;
	private static BufferedImage[] turfSprites;
	private static BufferedImage[] mobSprites;
	private static BufferedImage[] itemSprites;
	private static BufferedImage[] effectSprites;
	
	public static void loadResources() {
		
		turfSprites = new BufferedImage[10];
		mobSprites = new BufferedImage[10];
		itemSprites = new BufferedImage[10];
		effectSprites = new BufferedImage[10];
		
		for(int i = 0; i < 5; i++) {
			try{
				file = new File("tile" + i + ".png");
				turfSprites[i] = ImageIO.read(file);
			}catch(IOException e){
				System.out.println(e);
			}
		}
		
		File folder = new File("assets/textures/mobs");
		File[] listOfFiles = folder.listFiles();
		int index = 0;

		for (File file : listOfFiles) {
		    if (file.isFile()) {
				try{
					mobSprites[index] = ImageIO.read(file);
					index++;
				}catch(IOException e){
					System.out.println(e);
				}
		    }
		}
		
		folder = new File("assets/textures/items");
		listOfFiles = folder.listFiles();
		index = 0;

		for (File file : listOfFiles) {
		    if (file.isFile()) {
				try{
					itemSprites[index] = ImageIO.read(file);
					index++;
				}catch(IOException e){
					System.out.println(e);
				}
		    }
		}
		
		folder = new File("assets/textures/effects");
		listOfFiles = folder.listFiles();
		index = 0;

		for (File file : listOfFiles) {
		    if (file.isFile()) {
				try{
					effectSprites[index] = ImageIO.read(file);
					index++;
				}catch(IOException e){
					System.out.println(e);
				}
		    }
//		    System.out.println(effectSprites.length);
		}
		//for(int i = 0; i < 1; i++) {
//			try{
//				file = new File("tile3.png");
//				mobSprites[0] = ImageIO.read(file);
//
//			}catch(IOException e){
//				System.out.println(e);
//			}
//			
//			try{
//				file = new File("assets/textures/effects/bullets.png");
//				effectSprites[0] = ImageIO.read(file);
//
//			}catch(IOException e){
//				System.out.println(e);
//			}
		//}
		
		loadStatuses();
	}
	
	public static void loadStatuses() {
		
		try {
			file = new File("assets/data/statuses.txt");
			Scanner sc = new Scanner(file);
			
		    while (sc.hasNextLine()) {
		    	String statusLine = sc.nextLine();
		        String[] data = statusLine.split(",");
		        Status status = new Status(data[0], data[1], Boolean.parseBoolean(data[2]));
		        		
		        SSStatus.statuses.add(status);
		    } 
		    sc.close();
		
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	public static BufferedImage loadImage(String path) {
		
		try{
			file = new File(path);
			return ImageIO.read(file);

		}catch(IOException e){
			System.out.println(e);
			return null;
		}
	}
	
	public static BufferedImage getSprite(int type, int typeClass, int state, int animStep) {
		
		switch (type) {
		case DEFINES.TURF:
			return turfSprites[typeClass];
		case DEFINES.MOB:
			return getSubImage(mobSprites[typeClass], state, animStep);
		case DEFINES.ITEM:
			return getSubImage(itemSprites[typeClass], state, animStep);
		case DEFINES.EFFECT:
			return getSubImage(effectSprites[typeClass], state, animStep);
		default:
			System.out.println("WARNING: Sprite type not found");
			return null;
		}
	}
	
	private static BufferedImage getSubImage(BufferedImage spriteSheet, int state, int animStep) {
		
		return spriteSheet.getSubimage(animStep * Game.pixelRes, 0, Game.pixelRes, Game.pixelRes);
	}
}