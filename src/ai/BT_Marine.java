package ai;

import entity.mob.Mob;
import game.DEFINES;

public class BT_Marine extends BehavourTree {

	public BT_Marine(Mob mob) {
		super(mob);
	}
	
	public void process() {
		if(status == DEFINES.IDLE) {
			new AITask_CheckForTargets(mob);
		}
		
		if(status == DEFINES.COCKING) {
			new AITask_CheckIfCocked(mob);
		}
		
		if(status == DEFINES.DEFENDING) {
			new AITask_AttackTarget(mob);
		}
		
		if(status == DEFINES.RELOADING) {
			new AITask_CheckReload(mob);
		}
	}
}