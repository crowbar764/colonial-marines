package ai;

import java.util.ArrayList;

import entity.Entity;
import entity.mob.Mob;
import entity.turf.Turf;
import game.DEFINES;
import game.Game;
import world.World;

public class AITask_CheckForTargets {

	public AITask_CheckForTargets(Mob mob) {
		
		
		int range = mob.range;
		int mobGridX = mob.x / Game.pixelRes;
		int mobGridY = mob.y / Game.pixelRes;
		Entity closestTarget = null;
		double closestTargetDistance = 9999;
		
		for(int y = mobGridY - range; y < mobGridY + range + 1; y++) {
			for(int x = mobGridX - range; x < mobGridX + range + 1; x++) {
				
				// If searching out of map.
				if(x < 0 || y < 0 || x > World.width - 1 || y > World.height - 1) {
					continue;
				}
				
				Turf turf = World.getTurf(x, y);
				ArrayList<Entity> occupants = turf.getOccupants();
				double distance = Math.sqrt(Math.abs(x - mobGridX) + Math.abs(y - mobGridY));
				// TODO check / type, can be destructed
				for(Entity occupant : occupants) {
					if(occupant.faction != mob.faction && distance < closestTargetDistance) {
						closestTarget = occupant;
						closestTargetDistance = distance;
					}
				}
			}
		}
		
		if(closestTarget != null) {
			mob.target = closestTarget;
			mob.ai.status = DEFINES.DEFENDING;
		}
	}
}