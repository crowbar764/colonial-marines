package ai;

import entity.mob.Mob;
import game.DEFINES;

public class AITask_AttackTarget {
	
	public AITask_AttackTarget(Mob mob) {
		mob.targetsX = mob.target.x;
		mob.targetsY = mob.target.y;
		mob.targetsDirection = Math.toDegrees(Math.atan2(mob.targetsY - mob.y, mob.targetsX - mob.x));
		mob.shoot();
		mob.currentMag--;
		mob.ai.status = DEFINES.COCKING;
		
		if(mob.currentMag == 0) {
			mob.reloadStartTime = System.nanoTime();
			mob.ai.status = DEFINES.RELOADING;
		}
	}
}