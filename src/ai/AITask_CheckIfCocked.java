package ai;

import entity.mob.Mob;
import game.DEFINES;

public class AITask_CheckIfCocked {
	
	public AITask_CheckIfCocked(Mob mob) {
		if((System.nanoTime() - mob.lastShotTime) / mob.shotCooldown > 1 ) {
			System.out.println("dd");
			mob.lastShotTime = System.nanoTime();
			
			mob.ai.status = DEFINES.IDLE;
		}
	}
}