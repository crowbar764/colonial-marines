package ai;

import entity.mob.Mob;
import game.DEFINES;

public class AITask_CheckReload {
	public AITask_CheckReload(Mob mob) {
		if((System.nanoTime() - mob.reloadStartTime) / mob.reloadTime > 1 ) {
			mob.currentMag = mob.magSize;
			mob.ai.status = DEFINES.COCKING;
		}
	}
}