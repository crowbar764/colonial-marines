package ai;

import entity.mob.Mob;
import game.DEFINES;

public class BehavourTree {
	
	public Mob mob;
	public int status;
	
	public BehavourTree(Mob mob) {
		this.mob = mob;
		status = DEFINES.IDLE;
	}
	
	public void process() {
	}
}