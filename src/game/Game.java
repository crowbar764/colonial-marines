package game;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import audio.SSAudio;
import entity.effect.Effect;
import entity.item.Item;
import entity.mob.Mob;
import entity.turf.Turf;
import io.Keyboard;
import io.Mouse;
import io.MouseDrag;
import io.MouseWheel;
import io.SSSelect;
import lighting.SSLighting;
import render.GUI;
import render.Renderer;
import render.ResourceManager;
import status.SSStatus;
import world.Camera;
import world.World;

public class Game {
	
	public static JFrame frame;
	public static Renderer renderer;
	
	public static Dimension screenDims, aspectRatio;
	public static int resX, resY, pixelRes, startingZoom;
	public static boolean running;
	
	public Game() {

		resX = 1280;
		resY = 720;
		pixelRes = 32;
		aspectRatio = new Dimension(16, 9);
		startingZoom = 2;
		
		renderer = new Renderer();
		
		ResourceManager.loadResources();
//		SSStatus.init();
		GUI.init();
		createElements();
		renderer.init();
		GUI.syncCheckboxes();
		
		
		
		World.loadMap("map1.png");
		Camera.init();
		
		
		
		//SSAtmos.start();
		
		SSSelect.init();
		SSAudio.init();
		SSLighting.init();
		
		
		startGameLoop();
	}

	private void createElements() {
		
		screenDims = Toolkit.getDefaultToolkit().getScreenSize();
	    int windowOffsetX = (int)((screenDims.getWidth() - resX) / 2);
	    int windowOffsetY = (int)((screenDims.getHeight() - resY) / 2);
	    
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocation(windowOffsetX, windowOffsetY);
		frame.setTitle("(not) Colonial Marines");
		
		JPanel panel = new JPanel(null);

		renderer.setSize(resX, resY);
		renderer.addMouseListener(new Mouse());
		renderer.addMouseMotionListener(new MouseDrag());
		renderer.addMouseWheelListener(new MouseWheel());
		renderer.addKeyListener(new Keyboard());
		panel.add(renderer);
		
		panel.setComponentZOrder(GUI.selectionPanel, 0);

		frame.pack();
		Insets insets = frame.getInsets();
		frame.setSize(resX + insets.left + insets.right, resY + insets.top + insets.bottom);
	    frame.add(panel);
		
		frame.setVisible(true);
//		frame.toFront();
		
		
	}
	
	private void startGameLoop() {
		
		renderer.requestFocus();
		
		running = true;
		double time = System.nanoTime();
		double aiTime = System.nanoTime();
		double mobTime = System.nanoTime();
		double mobFastTime = System.nanoTime();
		double effectTime = System.nanoTime();
		double effectFastTime = System.nanoTime();
		
		while(running) {
			if((System.nanoTime() - time) / 10000000l > 1) {
				time = System.nanoTime();
				
				for(Turf[] turfRow : World.turfs) {
					for(Turf turf : turfRow) {
						turf.process();
					}
				}
				
				for(Mob mob : World.mobs) {
					mob.ai.process();
				}
				
//				if((System.nanoTime() - aiTime) / DEFINES.AITICKRATE > 1) {
//					aiTime = System.nanoTime();
//					
//					for(Mob mob : World.mobs) {
//						mob.ai.process();
//					}
//				}
				
				if((System.nanoTime() - mobTime) / DEFINES.MOBTICKRATE > 1) {
					mobTime = System.nanoTime();
					for(Mob mob : World.mobs) {
						mob.process();
					}
					
					for(Item item : World.items) {
						item.process();
					}
				}
				
				if((System.nanoTime() - mobFastTime) / DEFINES.MOBFASTTICKRATE > 1) {
					mobFastTime = System.nanoTime();
					for(Mob mob : World.mobs) {
						mob.fastProcess();
					}
				}
				
				if((System.nanoTime() - effectTime) / DEFINES.EFFECTTICKRATE > 1) {
					effectTime = System.nanoTime();
					ArrayList<Effect> effects = World.effects;
					for(int i = 0; i < effects.size(); i++) {
						effects.get(i).process();
					}
//					for(Effect effect : World.effects) {
//						effect.process();
//					}
//					System.out.println(Mouse.getWorldMousePos());
				}
				
				if((System.nanoTime() - effectFastTime) / DEFINES.FASTTICKRATE > 1) {
					effectFastTime = System.nanoTime();
					ArrayList<Effect> effects = World.effects;
					for(int i = 0; i < effects.size(); i++) {
						effects.get(i).fastProcess();
					}
					
					SSAudio.process();
					Camera.process();
				}
				
				Keyboard.process();
				
				if(SSLighting.needsUpdate) {
					SSLighting.update();
				}
				
				renderer.render();
			}
		}
	}
	
	public static void main(String[] args) {
		new Game();
	}
}