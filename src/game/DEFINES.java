package game;

import java.awt.Color;

public class DEFINES {
	
	public static final long ATMOSTICKRATE = 	250000000l;
	public static final long AITICKRATE = 		3000000000l;
	public static final long MOBTICKRATE = 		300000000l;
	public static final long MOBFASTTICKRATE =	150000000l;
	public static final long CAMERATICKRATE = 	10000000l;
	public static final long EFFECTTICKRATE = 	250000000l;
	public static final long FASTTICKRATE = 	10000000l;
	
	//public static final long ASECOND = 1000000000;
	//public static final long QUARTERSECOND = 250000000;
	/*
	 * Factions
	 */
	public static final int WORLD = 0;
	public static final int PLAYER = 1;
	public static final int XENOS = 2;
	
	/*
	 * ZLevels
	 */
	public static final int TURF = 0;
	public static final int MOB = 1;
	public static final int EFFECT = 2;
	public static final int ITEM = 3;

	/*
	 * Key colours
	 */
	public static final int _SPACE = -16777216; //black
	public static final int _HULL = -3947581; //grey
	public static final int _PLATING = -1; //white
	public static final int _VENT = -32985;
	
	public static final int SPACE = 0;
	public static final int HULL = 1;
	public static final int PLATING = 2;
	public static final int VENT = 3;

	
	/*
	 * Mobs
	 */
	public static final int MARINE = 0;
	public static final int ENGINEER = 1;
	public static final int PILOT = 2;
	public static final int RUNNER = 3;
	public static final int MARKSMAN = 4;
	
	/*
	 * Items
	 */
	public static final int LIGHT = 0;
	
	/**
	 * Colours
	 */
	public static final Color RED= new Color(255, 76, 30);
	public static final Color GREEN = new Color(0, 200, 0);
	public static final Color WHITE = new Color(255, 255, 255);
	public static final Color BLUEGREY = new Color(52, 73, 94);
	public static final Color BLUEGREYDARK = new Color(44, 62, 80);
	
	/**
	 * AI states
	 */
	public static final int IDLE = 0;
	public static final int BUSY = 1;
	public static final int DEFENDING = 2;
	public static final int COCKING = 3;
	public static final int RELOADING = 4;
	
//	public enum Status22 {
//	    FOO1,
//	    FOO2,
//	    FOO3;
//	}
}

	

/**
	//CRITICAL//
	TOFIX - Rare selection crash.
	
	//TO DO//
	TODO - GUI component heights are not perfect.
	TODO - GUI statuses should be aligned to top of status panel.
	TODO - Holding shift and clicking adds mouse elements to selection.
	TODO - Give dynamic positioned entities collision with navigation calculation and add 'next best' goto spot.
	TODO - Collision of projectiles tracks corner intercepts which means collisions on the side only are ignored.
**/